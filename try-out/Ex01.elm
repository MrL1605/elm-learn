

import String
import Html


makeUpper name =
    String.length name >= 11


doUpper name =
    if makeUpper name then
        String.toUpper name
    else
        String.toLower name

getLen name=
    toString (String.length name)


getStr name =
    String.concat [ doUpper name, " name length:", getLen name ]


main =
    Html.text (getStr "Hi Lalit")

