module Main exposing (..)
import Html


type alias Item =
    { name : String, qty : Int, freeQty : Int }


cart: List Item
cart =
    [ { name = "Lemon", qty = 1, freeQty = 0 }
    , { name = "Apple", qty = 5, freeQty = 0 }
    , { name = "Pear", qty = 10, freeQty = 0 }
    , { name = "maybe", qty = 1100, freeQty = 0 }
    , { name = "Something", qty = 6, freeQty = 0 }
    ]

addFreebies: Int -> Int -> Item -> Item
addFreebies minQty amt item =
    if item.qty >= minQty && item.freeQty == 0 then
        { item | freeQty = amt }
    else
        item

main =
    List.map ((addFreebies 10 3) >> (addFreebies 5 1)) cart
        |> toString
        |> Html.text

