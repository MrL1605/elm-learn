module Update exposing (..)

import Model


type Msg
    = SetCurrentText String
    | ToggleDirection
    | UpdateSelected String


update : Msg -> Model.Model -> Model.Model
update msg model =
    case msg of
        SetCurrentText newText ->
            -- currently, this does nothing!
            { model | currentText = newText }

        ToggleDirection ->
            case model.direction of
                Model.TextToEmoji ->
                    { model | direction = Model.EmojiToText }

                Model.EmojiToText ->
                    { model | direction = Model.TextToEmoji }

        UpdateSelected emoji ->
            { model | selectedEmoji = emoji }
