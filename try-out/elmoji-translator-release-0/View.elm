module View exposing (..)

import Array
import EmojiConverter
import Update
import Model
import Html
import Html.Events
import Html.Attributes exposing (..)


translateText : Model.Model -> String
translateText model =
    case model.direction of
        Model.TextToEmoji ->
            (EmojiConverter.textToEmoji model.selectedEmoji model.currentText)

        Model.EmojiToText ->
            (EmojiConverter.emojiToText model.selectedEmoji model.currentText)


view : Model.Model -> Html.Html Update.Msg
view model =
    Html.div
        [ style [ ( "height", "100%" ) ], class "blue-grey darken-2" ]
        [ Html.node "link"
            [ rel "stylesheet"
            , href "stylesheets/main.css"
            ]
            []
        , Html.nav
            []
            [ Html.div
                [ class "nav-wrapper light-blue lighten-2" ]
                [ Html.div
                    [ class "brand-logo center" ]
                    [ Html.text "Elmoji Translator" ]
                ]
            ]
        , Html.section
            [ class "container" ]
            [ Html.div
                [ class "input-field" ]
                [ Html.input
                    [ type_ "text"
                    , class "center"
                    , placeholder "Let's Translate!"
                    , Html.Events.onInput Update.SetCurrentText
                    ]
                    []
                ]
            , Html.div [ class "switch center" ]
                [ Html.label []
                    [ Html.text "Translate Text"
                    , Html.input
                        [ type_ "checkbox", Html.Events.onClick Update.ToggleDirection ]
                        []
                    , Html.span [ class "lever" ] []
                    , Html.text "Translate Emoji"
                    ]
                ]
            , Html.p
                [ class "center output-text emoji-size" ]
                [ Html.text (translateText model) ]
            , Html.div [ class "divider" ] []
            , Html.section [ class "container" ]
                [ Html.div [ class "row" ] (getEmojiBoxes model)
                ]
            ]
        ]


getEmojiBoxes : Model.Model -> List (Html.Html Update.Msg)
getEmojiBoxes model =
    List.map
        (\emoji ->
            Html.div [ class "col s2 m1 emoji-size", Html.Events.onClick (Update.UpdateSelected emoji) ]
                [ Html.div [ classList [ ( "key-selector", True ), ( "is-selected", emoji == model.selectedEmoji ) ] ] [ Html.text emoji ]
                ]
        )
        EmojiConverter.supportedEmojis
