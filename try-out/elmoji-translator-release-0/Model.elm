module Model exposing (..)

import EmojiConverter


type alias Model =
    { currentText : String
    , direction : Direction
    , selectedEmoji : String
    }


type Direction
    = EmojiToText
    | TextToEmoji


init : Model
init =
    { currentText = ""
    , direction = TextToEmoji
    , selectedEmoji = defaultKey
    }


defaultKey : String
defaultKey =
    (Maybe.withDefault "" (List.head EmojiConverter.supportedEmojis))
