module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


-- model


type alias Model =
    { total : Int
    , input : Int
    }


initModel : Model
initModel =
    { total = 0, input = 0 }



-- update


type Msg
    = AddCalorie String
    | SetCalorie
    | Clear


update : Msg -> Model -> Model
update msg model =
    case msg of
        SetCalorie ->
            { model | total = model.total + model.input, input = 0 }

        AddCalorie inputVal ->
            case String.toInt inputVal of
                Ok val ->
                    { model | input = val }

                Err err ->
                    { model | input = 0 }

        Clear ->
            initModel



-- view


view : Model -> Html Msg
view model =
    div []
        [ h3 []
            [ text ("Total Calories: " ++ (toString model)) ]
        , input
            [ type_ "number"
            , onInput AddCalorie
            , value
                (if model.input == 0 then
                    ""
                 else
                    toString model.input
                )
            ]
            []
        , button
            [ type_ "button"
            , onClick SetCalorie
            ]
            [ text "Add" ]
        , button
            [ type_ "button"
            , onClick Clear
            ]
            [ text "Clear" ]
        ]


main : Program Never Model Msg
main =
    Html.beginnerProgram
        { model = initModel
        , update = update
        , view = view
        }
