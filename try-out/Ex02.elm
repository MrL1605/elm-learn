module Main exposing (..)

import Html
import String


getHead str =
    String.left 1 str


(~=) a b =
    getHead a == getHead b


wordCount str =
    List.length (String.split " " str)


main =
    {-
       (~=) "Lal" "lalit"
           |> toString
           |> Html.text
    -}
    wordCount "Count the number of words in this mother fucking line bitch"
        |> toString
        |> Html.text
